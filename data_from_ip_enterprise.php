<?php
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * 1. In the same directory as this script, download https://github.com/maxmind/GeoIP2-php/releases/download/v2.11.0/geoip2.phar
 * 2. In the same directory as this script, download/move the GeoIP2 Enterprise databases (the .mmdb files)
 * 3. Run: `php data_from_ip_enterprise.php <ip address>` (or if you use docker, instead run: `docker-compose exec mediawiki php data_from_ip_enterprise.php <ip address>`)
 */

include 'geoip2.phar';

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;

$ip = $argv[1];
$info = ['IP' => $ip];

$enterpriseReader = new Reader( 'GeoIP2-Enterprise.mmdb' );
$anonymousIpReader = new Reader( 'GeoIP2-Anonymous-IP.mmdb' );

try {
	$enterpriseInfo = $enterpriseReader->enterprise( $ip );
	$anonymousIpInfo = $anonymousIpReader->anonymousIp( $ip );
} catch ( AddressNotFoundException $e ) {
	quit();
}

$info['latitude'] = $enterpriseInfo->location->latitude;
$info['longitude'] = $enterpriseInfo->location->longitude;

$info['asn'] = $enterpriseInfo->traits->autonomousSystemNumber;
$info['organization'] = $enterpriseInfo->traits->autonomousSystemOrganization;

$info['country_geoid'] = $enterpriseInfo->country->geonameId;
$info['country_name'] = $enterpriseInfo->country->names;

$info['city_geoid'] = $enterpriseInfo->city->geonameId;
$info['city_name'] = $enterpriseInfo->city->names;
$info['subdivisions'] = array_map(
	static function ( $subdivision ) {
		return $subdivision->names;
	},
	array_reverse( $enterpriseInfo->subdivisions )
);

$info['isp'] = $enterpriseInfo->traits->isp;
$info['connectionType'] = $enterpriseInfo->traits->connectionType;
$info['userType'] = $enterpriseInfo->traits->userType;

$info['isAnonymous'] = $anonymousIpInfo->isAnonymous;
$info['isAnonymousVpn'] = $anonymousIpInfo->isAnonymousVpn;
$info['isPublicProxy'] = $anonymousIpInfo->isPublicProxy;
$info['isResidentialProxy'] = $anonymousIpInfo->isResidentialProxy;
$info['isLegitimateProxy'] = $anonymousIpInfo->isLegitimateProxy ?? null;
$info['isTorExitNode'] = $anonymousIpInfo->isTorExitNode;
$info['isHostingProvider'] = $anonymousIpInfo->isHostingProvider;

var_export( $info );
