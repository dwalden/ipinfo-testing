<?php
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * This script will find IP addresses which return different datatypes from
 * MaxMind. This could be used, for example, to test how IPInfo deals with
 * null/empty/zero values.
 *
 * This is for MaxMind's GeoIP2 Enterprise databases.
 * 
 * It will return a CSV. Each row has a different IP address plus all the
 * "expected" values for that IP (based on what MaxMind returns). Each IP has a
 * more-or-less unique combination of return values for each data point we are
 * interested in (e.g. ISP, ASN, Connection Type).
 * 
 * 1. In the same directory as this script, download https://github.com/maxmind/GeoIP2-php/releases/download/v2.11.0/geoip2.phar
 * 2. Download and extract the GeoIP2 Enterprise databases (called "GeoIP2 Anonymous IP" and "GeoIP2 Enterprise") and move the .mmdb files into the same directory
 * 3. Download and extract "GeoIP2 Enterprise: CSV format" and move the .csv files into the same directory
 * 4. Run: `php erd_enterprise.php <input csv> <output csv>` (or if you use docker, instead run: `docker-compose exec mediawiki php erd_enterprise.php <input csv> <output csv>`)
 *    - Where `<input csv>` is one of either `GeoIP2-Enterprise-Blocks-IPv4.csv` or `GeoIP2-Enterprise-Blocks-IPv6.csv`
 *    - `<output csv>` is where the results will be written and can be called anything
 */

include 'geoip2.phar';

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;


$enterpriseReader = new Reader( 'GeoIP2-Enterprise.mmdb' );
$anonymousIpReader = new Reader( 'GeoIP2-Anonymous-IP.mmdb' );

$forComparison = [];
$forPrint = [];

$geoip2csv = $argv[1];
$outputcsv = $argv[2];

$file = fopen($geoip2csv, "r");

// Ignore header
$data = fgetcsv($file, 1000, ",");

$i = 0;

// This stops after going through a million values. This is arbitrary, and could
// be increased or decreased.
while (($data = fgetcsv($file, 1000, ",")) !== FALSE && $i < 100000) {
	$info = [];

    $range = $data[0];
	$ip = explode("/", $range)[0];

	try {
		$enterpriseInfo = $enterpriseReader->enterprise( $ip );
		$anonymousIpInfo = $anonymousIpReader->anonymousIp( $ip );
	} catch ( AddressNotFoundException $e ) {
		continue;
	}

	$info['ip'] = $ip;

	$info['latitude'] = $enterpriseInfo->location->latitude;
	$info['longitude'] = $enterpriseInfo->location->longitude;

	$info['asn'] = $enterpriseInfo->traits->autonomousSystemNumber;
	$info['organization'] = $enterpriseInfo->traits->autonomousSystemOrganization;

	$info['country_geoid'] = $enterpriseInfo->country->geonameId;
	$info['country_name'] = $enterpriseInfo->country->name;

	$info['city_geoid'] = $enterpriseInfo->city->geonameId;
	$info['city_name'] = $enterpriseInfo->city->name;
	$info['subdivisions'] = array_map(
		static function ( $subdivision ) {
			return $subdivision->name;
		},
		array_reverse( $enterpriseInfo->subdivisions )
	);

	$info['isp'] = $enterpriseInfo->traits->isp;
	$info['connectionType'] = $enterpriseInfo->traits->connectionType;
	$info['userType'] = $enterpriseInfo->traits->userType;

	$info['isAnonymous'] = $anonymousIpInfo->isAnonymous;
	$info['isAnonymousVpn'] = $anonymousIpInfo->isAnonymousVpn;
	$info['isPublicProxy'] = $anonymousIpInfo->isPublicProxy;
	$info['isResidentialProxy'] = $anonymousIpInfo->isResidentialProxy;
	$info['isLegitimateProxy'] = $anonymousIpInfo->isLegitimateProxy ?? null;
	$info['isTorExitNode'] = $anonymousIpInfo->isTorExitNode;
	$info['isHostingProvider'] = $anonymousIpInfo->isHostingProvider;

	$add = TRUE;
	foreach ( $forComparison as $compare ) {
		if (
			( ( is_float( $info['latitude'] ) && is_float( $compare['latitude'] ) ) || $info['latitude'] === $compare['latitude'] )
			&&
			( ( is_float( $info['longitude'] ) && is_float( $compare['longitude'] ) ) || $info['longitude'] === $compare['longitude'] )
			&&
			( ( is_int( $info['asn'] ) && is_int( $compare['asn'] ) ) || $info['asn'] === $compare['asn'] )
			&&
			( ( is_string( $info['organization'] ) && is_string( $compare['organization'] ) ) || $info['organization'] === $compare['organization'] )
			&&
			( ( is_int( $info['country_geoid'] ) && is_int( $compare['country_geoid'] ) ) || $info['country_geoid'] === $compare['country_geoid'] )
			&&
			( ( is_string( $info['country_name'] ) && is_string( $compare['country_name'] ) ) || $info['country_name'] === $compare['country_name'] )
			&&
			( ( is_int( $info['city_geoid'] ) && is_int( $compare['city_geoid'] ) ) || $info['city_geoid'] === $compare['city_geoid'] )
			&&
			( ( is_string( $info['city_name'] ) && is_string( $compare['city_name'] ) ) || $info['city_name'] === $compare['city_name'] )
			&&
			( $info['subdivisions'] === $info['subdivisions'] || count( $info['subdivisions'] ) === count( $info['subdivisions'] ) )
			&&
			( ( is_string( $info['isp'] ) && is_string( $compare['isp'] ) ) || $info['isp'] === $compare['isp'] )
			&&
			( $info['connectionType'] === $compare['connectionType'] )
			&&
			( $info['userType'] === $compare['userType'] )
			&&
			( $info['isAnonymous'] === $compare['isAnonymous'] )
			&&
			( $info['isAnonymousVpn'] === $compare['isAnonymousVpn'] )
			&&
			( $info['isPublicProxy'] === $compare['isPublicProxy'] )
			&&
			( $info['isResidentialProxy'] === $compare['isResidentialProxy'] )
			&&
			( $info['isLegitimateProxy'] === $compare['isLegitimateProxy'] )
			&&
			( $info['isTorExitNode'] === $compare['isTorExitNode'] )
			&&
			( $info['isHostingProvider'] === $compare['isHostingProvider'] )
		) {
			$add = FALSE;
			break;
		}
	}
	if ( $add ) {
		$forComparison[] = $info;

		$forPrint[] = array_map(
			static function ( $item ) {
				return var_export( $item, TRUE );
			},
			$info
		);
	}
	$i = $i + 1;
}

$fp = fopen($outputcsv, 'w');

// write the header
fputcsv($fp, ['ip', 'latitude', 'longitude', 'asn', 'organization', 'country_geoid', 'country_name', 'city_geoid', 'city_name', 'subdivisions', 'isp', 'connectionType', 'userType', 'isAnonymous', 'isAnonymousVpn', 'isPublicProxy', 'isResidentialProxy', 'isLegitimateProxy', 'isTorExitNode', 'isHostingProvider']);

foreach ( $forPrint as $infoi ) {
    fputcsv($fp, $infoi);
}

fclose($fp);

var_export( $forComparison );

fclose($file);

