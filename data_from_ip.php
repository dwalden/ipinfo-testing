<?php
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * 1. In the same directory as this script, download https://github.com/maxmind/GeoIP2-php/releases/download/v2.11.0/geoip2.phar
 * 2. In the same directory as this script, download/move the GeoLite2 databases (the .mmdb files)
 * 3. Run: `php data_from_ip.php <ip address>` (or if you use docker, instead run: `docker-compose exec mediawiki php data_from_ip.php <ip address>`)
 */

include 'geoip2.phar';

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;

$ip = $argv[1];
$info = ['IP' => $ip];

try {

	$asnReader = new Reader( 'GeoLite2-ASN.mmdb' );
	$asn = $asnReader->asn( $ip );
	$info['ASN'] = $asn->autonomousSystemNumber;
	$info['ASO'] = $asn->autonomousSystemOrganization;

} catch ( AddressNotFoundException $e ) {

	$info['ASN'] = "";
	$info['ASO'] = "";

}

try {

	$cityReader = new Reader( 'GeoLite2-City.mmdb' );
	$city = $cityReader->city( $ip );
	$info['GeoNameID'] = $city->city->geonameId;
	$info['Name'] = $city->city->name;
	$info['Latitude'] = $city->location->latitude;
	$info['Longitude'] = $city->location->longitude;

	$subdivisions = array_map(
		function ( $subdivision ) {
			return [ "geonameid" => $subdivision->geonameId,
					 "name" => $subdivision->name ];
		},
		$city->subdivisions
	);
	foreach( $city->subdivisions as $subdivision ) {
		$info['Subdivisions'][] = [
			'GeoNameID' => $subdivision->geonameId ?? null,
			'Name' => $subdivision->name ?? null
		];
	}

} catch ( AddressNotFoundException $e ) {

	$info['GeoNameID'] = "";
	$info['Name'] = "";
	$info['Latitude'] = "";
	$info['Longitude'] = "";

}

try {

	$countryReader = new Reader( 'GeoLite2-Country.mmdb' );
	$country = $countryReader->country( $ip );
	$info[] = $country->country->isoCode;

} catch ( AddressNotFoundException $e ) {

	$info['CountryISOCode'] = "";

}

var_dump( $info );
