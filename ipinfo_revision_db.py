#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dom Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.


# This script will partition the revisions in the revision and archive table
# with respect to their permissions, return a random sample from each partition
# (by default 1), submit this to the IPInfo revision endpoint and record the
# response in a CSV file.

# It will also look up that same revision in the
# ?action=query&prop=revisions/deletedrevisions API and record this to the same
# CSV. Thus, you can compare the data the user can look up about a revision via
# IPInfo vs. the API.

# 1. Setup pywikibot using the instructions here https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Download and copy https://gitlab.wikimedia.org/dwalden/ipinfo-testing/-/blob/main/ipinfo_drevision_db.py into the `scripts/` directory in pywikibot
# 3. Run: `python3 pwb.py ipinfo_revision_db -s /path/to/local/mediawiki/sqlite_file.sqlite -lang:<lang> -family:<wiki>`

import argparse
import csv
import sqlite3
import re
import ipaddress

from datetime import datetime

import pywikibot
from pywikibot.data.mysql import mysql_query
from pywikibot.comms import http


class IPInfoRevisionDB(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=1)
        parser.add_argument('-s', '--sqlite', default=False)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        outputfile = "ipinfo_revision_{}_{}_{}_{}.csv".format(self.site.code, self.site.family, self.site.user(), datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))

        rows = []
        fields = ["rev_id", "Archived?", "Text Hidden", "Comment Hidden", "Editor Hidden", "Revision Suppressed", "Editor", "Editor Is IP", "Editor Blocked", "Editor Suppressed", "IPInfo IP", "API IP", "error", "httpCode", "httpReason", "organization", "country", "location", "isp", "connectionType", "userType", "proxyType"]

        query = """
WITH random_revisions AS
(SELECT foo.*, row_number() OVER(PARTITION BY `Archived?`, `Editor Suppressed`, `Editor Blocked`, `Editor Is IP`, `Editor Hidden`, `Revision Suppressed`, `Text Hidden`, `Comment Hidden` ORDER BY RANDOM()) AS random_sort FROM
(SELECT rev_id AS rev_id,
        FALSE AS "Archived?",
        CASE WHEN 1 & rev_deleted THEN "Yes" ELSE "No" END AS "Text Hidden",
        CASE WHEN 2 & rev_deleted THEN "Yes" ELSE "No" END AS "Comment Hidden",
        CASE WHEN 4 & rev_deleted THEN "Yes" ELSE "No" END AS "Editor Hidden",
        CASE WHEN 8 & rev_deleted THEN "Yes" ELSE "No" END AS "Revision Suppressed",
        actor_name AS Editor,
        CASE WHEN actor_name REGEXP "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|^[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+$" THEN "Yes" ELSE "No" END AS "Editor Is IP",
        CASE WHEN editor_block.ipb_id THEN "Yes" ELSE "No" END AS "Editor Blocked",
        CASE WHEN editor_block.ipb_deleted THEN "Yes" ELSE "No" END AS "Editor Suppressed"
FROM revision
INNER JOIN actor ON revision.rev_actor = actor.actor_id
LEFT JOIN ipblocks AS editor_block ON actor.actor_user = editor_block.ipb_user OR actor.actor_name = editor_block.ipb_address
UNION
SELECT ar_rev_id AS rev_id,
       TRUE AS "Archived?",
       CASE WHEN 1 & ar_deleted THEN "Yes" ELSE "No" END AS "Text Hidden",
       CASE WHEN 2 & ar_deleted THEN "Yes" ELSE "No" END AS "Comment Hidden",
       CASE WHEN 4 & ar_deleted THEN "Yes" ELSE "No" END AS "Editor Hidden",
       CASE WHEN 8 & ar_deleted THEN "Yes" ELSE "No" END AS "Revision Suppressed",
       actor_name AS Editor,
       CASE WHEN actor_name REGEXP "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|^[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+$" THEN "Yes" ELSE "No" END AS "Editor Is IP",
       CASE WHEN editor_block.ipb_id THEN "Yes" ELSE "No" END AS "Editor Blocked",
       CASE WHEN editor_block.ipb_deleted THEN "Yes" ELSE "No" END AS "Editor Suppressed"
FROM archive
INNER JOIN actor ON archive.ar_actor = actor.actor_id
LEFT JOIN ipblocks AS editor_block ON actor.actor_user = editor_block.ipb_user OR actor.actor_name = editor_block.ipb_address
) AS foo)
SELECT * FROM random_revisions WHERE random_sort <= {};
""".format(self.options.total)

        if self.options.sqlite:
            conn = sqlite3.connect(self.options.sqlite)
            conn.create_function('regexp', 2, lambda x, y: 1 if re.search(x,y) else 0)
            cursor = conn.cursor()
            cursor.execute(query)
            revisions = cursor.fetchall()
            conn.close()
        else:
            revisions = mysql_query(query, dbname=self.site.dbName())

        for revisionentry in revisions:
            row = {
                'rev_id': revisionentry[0],
                'Archived?': revisionentry[1],
                'Text Hidden': revisionentry[2],
                'Comment Hidden': revisionentry[3],
                'Editor Hidden': revisionentry[4],
                "Revision Suppressed": revisionentry[5],
                "Editor": revisionentry[6],
                "Editor Is IP": revisionentry[7],
                "Editor Blocked": revisionentry[8],
                "Editor Suppressed": revisionentry[9]
            }

            # Makes a request for logged in user
            response = http.session.get("{}://{}{}/rest.php/ipinfo/v0/revision/{}?dataContext=infobox&language=en".format(self.site.protocol(), self.site.family.langs[self.site.code],
                                                                                                                          self.site.scriptpath(), row['rev_id'])).json()

            api_request = self.site.simple_request(action='query', prop='revisions',
                                                   rvprop='user', revids=row['rev_id'])
            api_response = api_request.submit()

            row['API IP'] = False
            if 'pages' in api_response['query']:
                for page_id, page in api_response['query']['pages'].items():
                    if 'revisions' in page:
                        # row.update(page['revisions'][0])
                        if 'user' in page['revisions'][0]:
                            try:
                                # TODO: Normalise the IP address
                                row['API IP'] = ipaddress.ip_address(page['revisions'][0]['user'])
                            except:
                                row['API IP'] = page['revisions'][0]['user']

            api_request = self.site.simple_request(action='query', prop='deletedrevisions',
                                                   drvprop='user', revids=row['rev_id'])
            api_response = api_request.submit()

            if 'pages' in api_response['query']:
                for page_id, page in api_response['query']['pages'].items():
                    if 'deletedrevisions' in page:
                        # row.update(page['deletedrevisions'][0])
                        if 'user' in page['deletedrevisions'][0]:
                            try:
                                # TODO: Normalise the IP address
                                row['API IP'] = ipaddress.ip_address(page['deletedrevisions'][0]['user'])
                            except:
                                row['API IP'] = page['deletedrevisions'][0]['user']

            if 'messageTranslations' in response:
                row['error'] = response['messageTranslations']['en']
                row['httpCode'] = response['httpCode']
                row['httpReason'] = response['httpReason']
            elif 'message' in response:
                row['error'] = response['message']
            elif 'info' in response:
                row['IPInfo IP'] = response['info'][0]['subject']
                row.update(response['info'][0]['data']['ipinfo-source-geoip2'])

            rows.append(row)

        with open(outputfile, 'a', newline='') as csvfile:
            dictcsv = csv.DictWriter(csvfile, fields, restval=False, extrasaction="ignore")
            dictcsv.writeheader()
            dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = IPInfoRevisionDB(*args)
    app.run()


if __name__ == '__main__':
    main()
