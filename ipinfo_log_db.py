#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dom Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.


# This script will partition log items from the logging table with respect to
# their permissions, return a random sample from each partition (by default 1),
# submit this to the IPInfo log endpoint and record the response in a CSV file.

# It will also look up that same log in the ?action=query&list=logevents API and
# record this to the same CSV. Thus, you can compare the data the user can look
# up about a log item via IPInfo vs. the API.

# 1. Setup pywikibot using the instructions here https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
# 2. Download and copy https://gitlab.wikimedia.org/dwalden/ipinfo-testing/-/blob/main/ipinfo_log_db.py into the `scripts/` directory in pywikibot
# 3. Run: `python3 pwb.py ipinfo_log_db -s /path/to/local/mediawiki/sqlite_file.sqlite -lang:<lang> -family:<wiki>`

import argparse
import csv
import sqlite3
import re
import ipaddress

from datetime import datetime

import pywikibot
from pywikibot.data.mysql import mysql_query
from pywikibot.comms import http


class IPInfoLogDB(object):

    """IPInfoLogDB user, IP or IP range."""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', '--total', type=int, default=1)
        parser.add_argument('-s', '--sqlite', default=False)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()

        outputfile = "ipinfo_log_{}_{}_{}_{}.csv".format(self.site.code, self.site.family, self.site.user(), datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))

        rows = []
        fields = ["log_id", "Target Hidden", "Comment Hidden", "Performer Hidden", "Log Suppressed", "Performer", "Performer Is IP", "Performer Blocked", "Performer Suppressed", "Target", "Target Is IP", "Target Blocked", "Target Suppressed", "Action", "API performer", "API target", "IPInfo IP 1", "IPInfo IP 2", "timestamp", "error", "httpCode", "httpReason", "match performer", "match target"]

        query = """
WITH random_logs AS
(SELECT foo.*, row_number() OVER(PARTITION BY `Action`, `Performer Suppressed`, `Performer Blocked`, `Performer Is IP`, `Target Suppressed`, `Target Blocked`, `Target Is IP`, `Performer Hidden`, `Comment Hidden`, `Target Hidden`, `Log Suppressed` ORDER BY RANDOM()) AS random_sort FROM
(SELECT log_id,
        CASE WHEN 1 & log_deleted THEN "Yes" ELSE "No" END AS "Target Hidden",
        CASE WHEN 2 & log_deleted THEN "Yes" ELSE "No" END AS "Comment Hidden",
        CASE WHEN 4 & log_deleted THEN "Yes" ELSE "No" END AS "Performer Hidden",
        CASE WHEN 8 & log_deleted THEN "Yes" ELSE "No" END AS "Log Suppressed",
        actor_name AS Performer,
        CASE WHEN actor_name REGEXP "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|^[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+$" THEN "Yes" ELSE "No" END AS "Performer Is IP",
        CASE WHEN performer_block.ipb_id THEN "Yes" ELSE "No" END AS "Performer Blocked",
        CASE WHEN performer_block.ipb_deleted THEN "Yes" ELSE "No" END AS "Performer Suppressed",
        log_title AS Target,
        CASE WHEN log_title REGEXP "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$|^[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+\:[0-9A-Z]+$" THEN "Yes" ELSE "No" END AS "Target Is IP",
        CASE WHEN target_block.ipb_id THEN "Yes" ELSE "No" END AS "Target Blocked",
        CASE WHEN target_block.ipb_deleted THEN "Yes" ELSE "No" END AS "Target Suppressed",
        log_type AS Action,
        log_namespace,
        log_timestamp
FROM logging
INNER JOIN actor ON logging.log_actor = actor.actor_id
LEFT JOIN ipblocks AS performer_block ON actor.actor_user = performer_block.ipb_user
LEFT JOIN ipblocks AS target_block ON logging.log_title = target_block.ipb_address
) AS foo)
SELECT * FROM random_logs WHERE random_sort <= {};
""".format(self.options.total)

        # Get data from database
        if self.options.sqlite:
            conn = sqlite3.connect(self.options.sqlite)
            conn.create_function('regexp', 2, lambda x, y: 1 if re.search(x,y) else 0)
            cursor = conn.cursor()
            cursor.execute(query)
            logs = cursor.fetchall()
            conn.close()
        else:
            logs = mysql_query(query, dbname=self.site.dbName())

        for logentry in logs:
            row = {
                'log_id': logentry[0],
                'Target Hidden': logentry[1],
                'Comment Hidden': logentry[2],
                'Performer Hidden': logentry[3],
                "Log Suppressed": logentry[4],
                "Performer Is IP": logentry[6],
                "Performer Blocked": logentry[7],
                "Performer Suppressed": logentry[8],
                "Target Is IP": logentry[10],
                "Target Blocked": logentry[11],
                "Target Suppressed": logentry[12],
                "Action": logentry[13],
                "Namespace": logentry[14],
                "log_timestamp": logentry[15]
            }
            try:
                row['Performer'] = ipaddress.ip_address(logentry[5])
            except:
                row['Performer'] = logentry[5]
            try:
                row['Target'] = ipaddress.ip_address(logentry[9])
            except:
                row['Target'] = logentry[9]

            # Get data from API
            # Try to get API log via target
            # Return 5 rows because timestamp is not a unique identifier
            api_request = self.site.simple_request(action='query', list='logevents', lelimit=5, lestart=row['log_timestamp'],
                                                   letitle="{}{}".format(self.site.namespaces[row['Namespace']], row['Target']))
            api_response = api_request.submit()
            api_logs = api_response['query']['logevents']

            # Try to get API log via performer
            # Return 5 rows because timestamp is not a unique identifier
            api_request = self.site.simple_request(action='query', list='logevents', lelimit=5,
                                                   lestart=row['log_timestamp'], leuser=row['Performer'])
            api_response = api_request.submit()
            api_logs.extend(api_response['query']['logevents'])

            api_log = False
            for log in api_logs:
                if log['logid'] == row['log_id']:
                    api_log = log
                    break

            if api_log:
                try:
                    row['API performer'] = ipaddress.ip_address(api_log['user'])
                except:
                    row['API performer'] = api_log['user'] if 'user' in api_log else False
                try:
                    row['API target'] = ipaddress.ip_address(api_log['title'].replace("User:", ""))
                except:
                    row['API target'] = api_log['title'].replace("User:", "") if 'title' in api_log else False
            else:
                row['API performer'] = False
                row['API target'] = False                    

            # Get data from IPInfo
            ipinfo_response = http.session.get("{}://{}{}/rest.php/ipinfo/v0/log/{}?dataContext=infobox&language=en".format(self.site.protocol(), self.site.family.langs[self.site.code],
                                                                                                                            self.site.scriptpath(), row['log_id'])).json()
            row['timestamp'] = datetime.now().strftime("%Y%m%d%H%M%S")

            if 'messageTranslations' in ipinfo_response:
                row['error'] = ipinfo_response['messageTranslations']['en']
                row['httpCode'] = ipinfo_response['httpCode']
                row['httpReason'] = ipinfo_response['httpReason']
                row['IPInfo IP'] = False
            elif 'message' in ipinfo_response:
                row['error'] = ipinfo_response['message']
            elif 'info' in ipinfo_response:
                # TODO: Normalise the IP address
                row['IPInfo IP 1'] = ipaddress.ip_address(ipinfo_response['info'][0]['subject'])
                if len(ipinfo_response['info']) > 1:
                    row['IPInfo IP 2'] = ipaddress.ip_address(ipinfo_response['info'][1]['subject'])

            rows.append(row)

        with open(outputfile, 'a', newline='') as csvfile:
            dictcsv = csv.DictWriter(csvfile, fields, restval=False, extrasaction="ignore")
            dictcsv.writeheader()
            dictcsv.writerows(rows)


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = IPInfoLogDB(*args)
    app.run()


if __name__ == '__main__':
    main()
