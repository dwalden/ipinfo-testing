#!/usr/bin/python3

# Based on code in https://www.mediawiki.org/wiki/API:Edit#Python.

# MIT license

# This script simulates editing a wiki from any IP address you choose.
# 
# The wiki you want to edit must have in its LocalSettings.php:
#  $wgCdnServers = [ '$local_ip_address' ];
#  $wgUsePrivateIPs = true;
# where $local_ip_address is (I believe) your LAN IP address.
# 
# If you use docker, you might instead want to add:
#  $wgCdnServersNoPurge = [ '172.0.0.1/8' ];
#  $wgUsePrivateIPs = true;
#
# Then run: python3 edit_from_ip.py -u $url -i $ip [-t $title]
# where:
#  - $url is the URL of the wiki you want to test, including '/w/api.php'
#  - $ip is the IP you want to simulate an edit from
#  - $title is the page you want to edit (this is optional, if you don't
#    pass this parameter it will edit the Main_Page)
# 
# For example: python3 edit_from_ip.py -u http://localhost:8080/w/api.php -i 1.2.3.4

import requests
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-u", "--url", required=True, type=str,
                    help="URL of the wiki you want to edit, including '/w/api.php'")
parser.add_argument("-i", "--ip", required=True, type=str,
                    help="The IP address you want the edit to come from")
parser.add_argument("-t", "--title", required=False, default="Main_Page",
                    help="Page you want to edit")
args = parser.parse_args()

S = requests.Session()

# GET request to fetch CSRF token
PARAMS_2 = {
    "action": "query",
    "meta": "tokens",
    "format": "json"
}

R = S.get(url=args.url, params=PARAMS_2)
DATA = R.json()

CSRF_TOKEN = DATA['query']['tokens']['csrftoken']

# POST request to edit a page
PARAMS_3 = {
    "action": "edit",
    "title": args.title,
    "token": CSRF_TOKEN,
    "format": "json",
    "appendtext": "\n\nEdit from the edit_from_ip.py script"
}

R = S.post(args.url, data=PARAMS_3, headers={'X-Forwarded-For': args.ip})
DATA = R.json()

print(DATA)
